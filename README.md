## 选型

### 思路
- 使用typescript控制类型，方便开发时静态检查错误
- 使用next.js && react 作为框架，扩展性，功能性都不错
- 使用shadcn && tailwindcss 作为组件库和样式库。shadcn 的特点是复制代码，所以非常透明，可以自行修改。tailwindcss 是一个css库，也是非常透明实用，又可以方便利用已有的组件库和模板，快速开发
- axios 作为网络请求库，简单易用

### 具体技术选型

- 语言: typescript
- 框架: react && next.js
- 组件: shadcn && tailwindcss
- 网络请求: axios
- 多语言：i18next, react-i18next & 自定义i18next实例

`以下内容都是用 yarn 安装，如果使用 npm 请自行替换`

### 前端整体架构

- View && Controller:
	- react && next.js 作为基础UI框架，负责render
	- shadcn && tailwindcss 作为组件库和样式库，负责组件和样式
		- zod 负责表单验证
- Model && API:
	- api.ts 负责定义公共API接口，处理共用异常
	- apiSet/* 负责定义具体的API接口
	- const.ts 负责定义常量
	- models/* 负责定义数据模型，包括api接口中使用到的输入输出数据模型
- 优点
	- 代码结构清晰，易于维护
	- 调用api接口时，数据模型和常量都是类型安全的
	- 方便对api进行统一处理，如异常处理，统一的header，验证等

#### 实例解析

- src/app/login/page.tsx 负责登录页面的渲染
	- LoginForm 负责登录表单的渲染
	- formSchema 负责定义表单验证
	- onSubmit 负责表单提交和状态更新
- src/lib/apiSet/login.ts 负责定义登录API接口
	- src/lib/api.ts 调用公共的api接口定义
	- src/lib/const.ts 负责定义常量
	- src/lib/models/LoginResult.ts, LoginRequest.ts 负责定义登录接口的输入输出数据模型
- 登录流程
	- 用户输入用户名和密码 (app/login/page.tsx)
	- 用户点击登录按钮 (app/login/page.tsx)
	- 表单验证通过 (app/login/page.tsx formSchema)
	- 表单提交 (app/login/page.tsx onSubmit)
	- 调用登录API (lib/apiSet/login.ts, LoginResult.ts, LoginRequest.ts)
	- 返回登录结果 (lib/apiSet/login.ts, LoginResult.ts, LoginRequest.ts)

## 初始化 next.js

- `yarn create next-app@latest my-app --typescript --tailwind --eslint`

```
✔ Would you like to use `src/` directory? … No / Yes
✔ Would you like to use App Router? (recommended) … No / Yes
✔ Would you like to customize the default import alias (@/*)? … No / Yes
```

## 初始化 shadcn

- `npx shadcn-ui@latest init`

```
✔ Which style would you like to use? › New York
✔ Which color would you like to use as base color? › Slate
✔ Would you like to use CSS variables for colors? … no / yes
```

尝试加一个button

- `npx shadcn-ui@latest add button`

## 添加 Login 页

### 多语言包

next-i18next针对 app router有写一篇文章，并且有示例

- https://locize.com/blog/next-app-dir-i18n/
- https://github.com/i18next/next-app-dir-i18next-example-ts

原理是自己创建 i18next 实例

### 添加必要的组件

- `npx shadcn-ui@latest add <component>`

组件列表

- form
- input
- label

### 创建 page.tsx

创建 src/app/login/page.tsx，从模板中复制代码，其实就是简单的html


#### 参考tailwindcss模板

- https://tailwindcomponents.com/
- https://www.tailwindtoolbox.com/
- https://www.tailwindawesome.com/?price=free&type=template
- https://tailwindcomponents.com/component/tailwind-css-mobile-app-design

#### 创建Form

- 安装 zod , `yarn add zod`
- 使用 zod 来创建表单验证
```typescript
const formSchema = z.object({
	username: z.string().min(2).max(50),
	password: z.string().min(6).max(50),
  });
```
- 定义 Form
```typescript
	// 1. Define your form.
	const form = useForm<z.infer<typeof formSchema>>({
		resolver: zodResolver(formSchema),
		defaultValues: {
		  username: "",
		  password: "",
		},
	  })

	  // 2. Define a submit handler.
	  function onSubmit(values: z.infer<typeof formSchema>) {
		// Do something with the form values.
		// ✅ This will be type-safe and validated.
		console.log(values)
	  }
```

### 安装和配置 axios

#### 安装 axios

- `yarn add axios`

#### 定制自己的 axios

- 创建 src/lib/axios.ts ，包装一下
- 创建 src/lib/api.ts ，定义api接口
- 创建具体的API文件，如 src/lib/apiSet/login.ts

