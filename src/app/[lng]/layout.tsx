import type { Metadata } from "next";
import { Inter } from "next/font/google";
import { dir } from 'i18next'
import "../globals.css";
import { languages } from '../i18n/settings'

const inter = Inter({ subsets: ["latin"] });

export async function generateStaticParams() {
  return languages.map((lang) => ({ lang }))
}

export const metadata: Metadata = {
  title: "Hello UI",
  description: "a sample UI",
};

export default function RootLayout({
  children,
  params: {
    lng
  }
}: Readonly<{
  children: React.ReactNode;
  params: { lng: string }
}>) {
  return (
    <html lang={lng}>
      <head />
      <body className={inter.className}>{children}</body>
    </html>
  );
}
