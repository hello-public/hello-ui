import { postApi } from "../api";
import { LoginResult } from "../models/LoginResult";
import { LoginRequest } from "../models/LoginRequest";

export async function login(request: LoginRequest): Promise<LoginResult> {

	const result=await postApi('/login', request.toApiRequest());
	const loginResult = new LoginResult(result);

	return loginResult;
}
