import { callApi } from "../api";
import { CodeOnlyResult } from "../models/CodeOnlyResult";

export async function logout(username: string, password: string): Promise<CodeOnlyResult> {

	const result=await callApi('/logout', {username: username, password: password});
	const loginResult = new CodeOnlyResult(result);

	return loginResult;
}
