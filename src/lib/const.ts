
export const API_BASE_URL = process.env.NEXT_PUBLIC_API_URL || 'http://hello-service.test/api';

export enum StatusCode {
	SUCCESS = 200,
};

export enum ApiCode {
    CODE_SUCCESS = 1,

    CODE_ERROR_AUTH_FAILED=90001,
    CODE_ERROR_INTERNAL=99999,

};
