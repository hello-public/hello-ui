// 在组件或服务中
import axios from 'axios';
import { API_BASE_URL } from '@/lib/const';
import { ApiCode } from '@/lib/const';

// 创建 axios 实例
const axiosInstance = axios.create({
  // 可以在这里设置基础 URL 和其他全局配置
  baseURL: API_BASE_URL,
  headers: {
    'Content-Type': 'application/json',
    // 添加其他全局请求头
  },
});

// 添加请求拦截器
axiosInstance.interceptors.request.use(
  function (config) {
    // 在发送请求之前做些什么
    // 比如添加认证 token
    // config.headers.Authorization = `Bearer ${token}`;
    return config;
  },
  function (error) {
    // 对请求错误做些什么
    return Promise.reject(error);
  },
);

// 添加响应拦截器
axiosInstance.interceptors.response.use(
  function (response) {
    // 对响应数据做点什么
    return response;
  },
  function (error) {
    // 对响应错误做点什么
    // 可以在这里统一处理错误
    if (error.response) {
      // 请求已发出，服务器以状态码非 2xx 的形式响应
      console.error('Error status', error.response.status);
      // 可以根据 error.response.status 处理不同的错误情况
    } else if (error.request) {
      // 请求已发出，但没有收到响应
      console.error('No response received');
    } else {
      // 在设置请求时发生了某些事情，触发了一个错误
      console.error('Error', error.message);
    }

    return Promise.reject(error);
  },
);

export {axiosInstance};

/**
 * post api
 *
 * @param apiPath string
 * @param params {}
 */
export async function postApi(apiPath: string, params: {}) {
	try {
		const response = await axiosInstance.post(apiPath, params);
		console.log(response.data); // 处理响应数据

		// process code
		const code = response.data.code;
		if (code === ApiCode.CODE_SUCCESS) {
			// success
			return response.data;
		} else {
			// error
			console.error(response.data.error_message);
		}
	} catch (error) {
		// 错误已经在拦截器中统一处理了，这里可以处理特定于该请求的错误逻辑
		console.error(error);
	}
};

/**
 * fetch data , only for get method
 *
 * @param apiPath string
 * @param params array
 */
export async function fetchData(apiPath: string, params: []){
	try {
		const response = await axiosInstance.get(apiPath, { params });
		console.log(response.data); // 处理响应数据

		return response.data;
	} catch (error) {
		// 错误已经在拦截器中统一处理了，这里可以处理特定于该请求的错误逻辑
		console.error(error);
	}

	return null;
}
