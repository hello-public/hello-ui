
export class CodeOnlyResult{

	private code: number = 0;
	private success_message: string = '';
	private error_message: string = '';

	constructor (respData: any){
		this.init(respData);
	}

	public init(respData: any){
		this.code = respData.code;
		this.success_message = respData.success_message;
		this.error_message = respData.error_message;
	}

	public getCode(): number{
		return this.code;
	}

	public getSuccessMessage(): string{
		return this.success_message;
	}

	public getErrorMessage(): string{
		return this.error_message;
	}

}

