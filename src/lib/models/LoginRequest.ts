export class LoginRequest {
	private username: string = '';
	private password: string = '';

	constructor (username: string, password: string){
		this.username = username;
		this.password = password;
	}

	public getUsername(): string{
		return this.username;
	}

	public getPassword(): string{
		return this.password;
	}

	public toApiRequest(){
		return {username: this.username, password: this.password};
	}
}
