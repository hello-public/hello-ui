import {CodeOnlyResult} from "./CodeOnlyResult";

export class LoginResult extends CodeOnlyResult{

	private token: string = '';

	constructor (respData: any){
		super(respData);
		this.init(respData);
	}

	public init(respData: any){
		super.init(respData);
		this.token = respData.data.token;
	}

	public getToken(): string{
		return this.token;
	}

}
