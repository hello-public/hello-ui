// next-i18next.config.js
module.exports = {
	i18n: {
	  defaultLocale: 'en',
	  locales: ['en', 'cn'],
	},
	// 指定 locales 文件存放的文件夹，根据你的项目结构调整
	localePath: './public/locales',
};
